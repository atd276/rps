package rps;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;
import java.util.Random;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.crypto.spec.DESedeKeySpec;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


class Person{
    
    private String name;
     
    public void setName(String name){
        if(!name.isEmpty()){
            if(name.length()>10){
                name = name.substring(0, 11);
            }
            this.name = name.strip();  //removes spaces before and after
        }
    }
     
    public String getName(){
        return this.name;
    }
    
    public Person(String name){
        this.setName(name);  
    }
    
    public Person(){
        this("Ivan");
    }
    
    public Person(Person person){
        this(person.getName());
    }
    
    
    public String toString(){
        return(getName());
    }
    
}


class Player extends Person{
    private int move; // sets a value between 0 and 2 for rock, paper, scissors respectfully
    private int scores;
    
    public void setMove(int move){
        if(move>=0 && move<=2)this.move = move;
    }
    
    public void setScores(int scores){
        if(scores>0)this.scores = scores;
    }
    
    public int getMove(){
        return this.move;
    }
    
    public int getScores(){
        return this.scores;
    }

    public Player(String name, int move, int scores){   // konstruktor za obshto polzvane
        this.setName(name);
        this.setMove(move);  
        this.setScores(scores);
    }
    
    public Player(){                                    // konstruktor po podrazbirane
        this("Ivan", 0, 0);
    }
    
    public Player(Player player){                       // konstruktor za kopirane
        this(player.getName(), player.getMove(), player.getScores());
    }
    
    @Override
    public String toString(){
        return(String.format("%s: %d points", getName(), getScores()));
    }

}





public class visual extends javax.swing.JFrame implements KeyListener{  /*implements means you are using the elements 
    of a Java Interface in your class. extends means that you are creating a subclass of the base class you are extending.
    You can only extend one class in your child class, but you can implement as many interfaces as you would like.*/
    
    Random random = new Random();   
    
    
    public static Player player1 = new Player("Player1", 0 ,0);
    public static Player player2 = new Player("Player2", 0, 0);
    
 
    public static boolean player1_ready = false;
    public static boolean player2_ready = false;
    
    ImageIcon rock = new ImageIcon("images//rock.png");
    ImageIcon paper = new ImageIcon("images//paper.png");
    ImageIcon scissors = new ImageIcon("images//scissors.png");
     
    
    public visual() {
        initComponents();   
        Pattern name = Pattern.compile("^[A-Z][a-z]+$"); // regex
        
        // player 1 enters name
        boolean invalid = false;
        while(true){
            String name1;
            
            if(!invalid){
                name1 = JOptionPane.showInputDialog(new JFrame(),"Player 1 enter name(example: Bidon)");      
            }else{
                name1 = JOptionPane.showInputDialog(new JFrame(),"Player 1 enter a new name(a valid one)(example: Bidon)"); 
            }
            
            if (name1 == null){
                System.exit(0);
            }

            name1 = name1.strip();

            boolean matchFound1 = name.matcher(name1).find();
            if(matchFound1) {
                player1.setName(name1);
                break;
            } else {
                invalid = true;
            }
        }
       // player 2 enters name 
        
        invalid = false;
        while(true){
            String name2;
            
            if(!invalid){
                name2 = JOptionPane.showInputDialog(new JFrame(),"Player 2 enter name(example: Bidon)");      
            }else{
                name2 = JOptionPane.showInputDialog(new JFrame(),"Player 2 enter a new name(a valid one)(example: Bidon)"); 
            }
            
            if (name2 == null){
                System.exit(0);
            }

            name2 = name2.strip();

            boolean matchFound2 = name.matcher(name2).find();
            if(matchFound2) {
                player2.setName(name2);
                break;
            } else {
                invalid = true;
            }
        }
        
        
        player1_text.setText(player1.toString());
        player2_text.setText(player2.toString());
        rps_image_player1.setVisible(false);
        rps_image_player2.setVisible(false);
        this.addKeyListener(this);
        
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        
    }
    
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jButton1 = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        background = new javax.swing.JPanel();
        winning_text = new javax.swing.JLabel();
        rps_image_player1 = new javax.swing.JLabel();
        rps_image_player2 = new javax.swing.JLabel();
        player1_text = new javax.swing.JLabel();
        player2_text = new javax.swing.JLabel();

        jButton1.setText("jButton1");

        jLabel2.setText("jLabel2");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 100, Short.MAX_VALUE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 100, Short.MAX_VALUE)
        );

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setPreferredSize(new java.awt.Dimension(380, 260));
        setResizable(false);

        background.setPreferredSize(new java.awt.Dimension(380, 260));

        winning_text.setText("play (a,s,d;right,down,left)");

        player1_text.setText("player1: 0 points");
        player1_text.setVerticalAlignment(javax.swing.SwingConstants.TOP);

        player2_text.setText("player2: 0 points");
        player2_text.setVerticalAlignment(javax.swing.SwingConstants.TOP);

        javax.swing.GroupLayout backgroundLayout = new javax.swing.GroupLayout(background);
        background.setLayout(backgroundLayout);
        backgroundLayout.setHorizontalGroup(
            backgroundLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(backgroundLayout.createSequentialGroup()
                .addGroup(backgroundLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(rps_image_player1, javax.swing.GroupLayout.PREFERRED_SIZE, 180, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(player1_text, javax.swing.GroupLayout.PREFERRED_SIZE, 180, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(backgroundLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(rps_image_player2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(player2_text, javax.swing.GroupLayout.DEFAULT_SIZE, 180, Short.MAX_VALUE))
                .addContainerGap())
            .addGroup(backgroundLayout.createSequentialGroup()
                .addGap(114, 114, 114)
                .addComponent(winning_text, javax.swing.GroupLayout.PREFERRED_SIZE, 147, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        backgroundLayout.setVerticalGroup(
            backgroundLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, backgroundLayout.createSequentialGroup()
                .addGroup(backgroundLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(player1_text, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(player2_text, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(backgroundLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(rps_image_player1, javax.swing.GroupLayout.PREFERRED_SIZE, 165, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(rps_image_player2, javax.swing.GroupLayout.PREFERRED_SIZE, 165, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(winning_text)
                .addGap(41, 41, 41))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(background, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(background, javax.swing.GroupLayout.PREFERRED_SIZE, 249, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    
    
    public static String game_log = "\n########## NEW GAME ############\n";
    
    
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
    
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            System.out.println("ClassNotFoundException");
            //java.util.logging.Logger.getLogger(visual.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            System.out.println("InstantiationException");
            //java.util.logging.Logger.getLogger(visual.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            System.out.println("IllegalAccessException");
            //java.util.logging.Logger.getLogger(visual.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            System.out.println("UnsupportedLookAndFeelException");
            //java.util.logging.Logger.getLogger(visual.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        
        
        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new visual().setVisible(true);
                
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel background;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JLabel player1_text;
    private javax.swing.JLabel player2_text;
    private javax.swing.JLabel rps_image_player1;
    private javax.swing.JLabel rps_image_player2;
    private javax.swing.JLabel winning_text;
    // End of variables declaration//GEN-END:variables

    /**
     * When a textual key is pressed that can be transformed into 
     * valid Unicode char it generates keyTyped event.
     */
    @Override
    public void keyTyped(KeyEvent arg0) {
       
    }

    /**
     * The "key pressed" event. This event is generated when a key
     * is pushed down.
     */
    @Override
    public void keyPressed(KeyEvent arg0) {
        
        
        if(arg0.getKeyCode() == KeyEvent.VK_A && !player1_ready){
            player1.setMove(0);
            player1_ready = true;
            rps_image_player1.setIcon(rock);
        }
        else if(arg0.getKeyCode() == KeyEvent.VK_S && !player1_ready){
            player1.setMove(1);
            player1_ready = true;
            rps_image_player1.setIcon(paper);
        }
        else if(arg0.getKeyCode() == KeyEvent.VK_D && !player1_ready){
            player1.setMove(2);
            player1_ready = true;
            rps_image_player1.setIcon(scissors);
        }
        else if(arg0.getKeyCode() == KeyEvent.VK_LEFT && !player2_ready){
            player2.setMove(0);
            player2_ready = true;
            rps_image_player2.setIcon(rock);
        }
        else if(arg0.getKeyCode() == KeyEvent.VK_DOWN && !player2_ready){
            player2.setMove(1);
            player2_ready = true;
            rps_image_player2.setIcon(paper);
        }
        else if(arg0.getKeyCode() == KeyEvent.VK_RIGHT && !player2_ready){
            player2.setMove(2);
            player2_ready = true;
            rps_image_player2.setIcon(scissors);
        }
        
        
        if(player1_ready && player2_ready){ 
            switch(compareRPS(player1.getMove(), player2.getMove())){
                case 1:
                    player1.setScores(player1.getScores() + 1);
                    winning_text.setText(String.format("%s wins", player1.getName()));
                    player1_text.setText(player1.toString());
                    break;
                case 2:
                    player2.setScores(player2.getScores() + 1);
                    winning_text.setText(String.format("%s wins", player2.getName()));
                    player2_text.setText(player2.toString());
                    break;
                case 0:
                    winning_text.setText("draw");
                    break;
           
            }
            
            
            rps_image_player1.setVisible(true);
            rps_image_player2.setVisible(true);
            
            player1_ready = false;
            player2_ready = false;
            
            game_log += winning_text.getText() + "\n";
            
            int response = JOptionPane.showConfirmDialog(null, "Would you like to play again?", "Confirm", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
            if (response == JOptionPane.CLOSED_OPTION) {
                System.out.println("Closed by hitting the cross");
                System.exit(0);
            }
            if (response == JOptionPane.NO_OPTION) {
                
                File game_log_file = new File("gameLog.txt");
          
                try{
                    FileWriter myWriter = new FileWriter(game_log_file, true);
                    myWriter.write(game_log + "\n" + String.format("%s; %s", player1.toString(), player2.toString()));
                    myWriter.close();
                }catch(IOException e){
                    System.out.print("NESHTO SE EBA");
                }finally{
                    System.exit(0);
                }
            }
            
            
            rps_image_player1.setVisible(false);
            rps_image_player2.setVisible(false);
            winning_text.setText("play(a.s.d,left.down.right)"); 
            background.setBackground(new Color(100+random.nextInt(156), 100+random.nextInt(156), 100+random.nextInt(156)));
            
        }
        
               
    }

    /**
     * The "key released" event. This event is generated when a key
     * is let up.
     */
    @Override
    public void keyReleased(KeyEvent arg0) {
       
    }
    
    int compareRPS(int player1_move, int player2_move){
        switch(player1_move){
            case 0:
                switch(player2_move){
                    case 1:
                        return(2);               
                    case 2:
                        return(1);
                }
                break;
            case 1:
                switch(player2_move){
                    case 0:
                        return(1);
                    case 2:
                        return(2);
                }
                break;
            case 2:
                switch(player2_move){
                    case 0:
                        return(2);
                    case 1:
                        return(1);
                }
                break;
            default:
                return 0;
        }
        return 0;
    }
    
    
}
